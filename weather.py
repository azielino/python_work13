import sys
import os
from requests import request
import json
from datetime import datetime, timedelta


class Forecast:

    HISTORY = "history.txt"
    BASE_URL = "https://weatherapi-com.p.rapidapi.com/forecast.json"
    QUERYSTRING = {"q":"Wroclaw","days":"3"}
    
    def __init__(self, api_key=sys.argv[1], check_date=str(datetime.today().date() + timedelta(1))):
        self.api_key = api_key
        self.headers = {
        'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
        'x-rapidapi-key': f"{self.api_key}"
        }
        self.check_date = check_date
        self.history_info = self.check_history_file()

    def set_chance_of_rain(self):
        response = request("GET", self.BASE_URL , headers=self.headers, params=self.QUERYSTRING)
        response_json_dicts = json.loads(response.text)
        for api_dict in response_json_dicts['forecast']['forecastday']:
            if self.check_date == api_dict['date']:
                return api_dict['day']['daily_chance_of_rain']

    def add_forecast(self):
        chance_of_rain = self.set_chance_of_rain()
        with open(self.HISTORY, "a") as file:
            file.write(f"{self.check_date}\n{chance_of_rain}\n")
            if int(chance_of_rain) < 20:
                return "Nie bedzie padac"
            elif int(chance_of_rain) > 80:
                return "Bedzie padac"
            else:
                return "Nie wiem"

    def check_history_file(self):
        if os.path.exists(self.HISTORY):
            with open(self.HISTORY, 'r') as file:
                for row in file:
                    if self.check_date == row.replace("\n", ""):
                        chance_of_rain = int(file.readline().replace("\n", ""))
                        if chance_of_rain < 20:
                            return "Nie bedzie padac"
                        elif chance_of_rain > 80:
                            return "Bedzie padac"
                        else:
                            return "Nie wiem"

    def print_forecast(self):
            print(self.history_info) if self.history_info else print(self.add_forecast())


if len(sys.argv) == 2:
    my_forecast = Forecast()
else:
    my_forecast = Forecast(check_date=sys.argv[2])
my_forecast.print_forecast()